export interface DataCita {
    fecha:       string;
    hora:        string;
    motivo:      string;
    dentista:    string;
  }
  